# Recipe App

This is a Flutter app that displays a list of food recipes and allows users to view recipe details.

## Getting Started

These instructions will help you set up the project and run it on your local machine for development and testing purposes.

### Prerequisites

- Flutter SDK: [Installation Guide](https://flutter.dev/docs/get-started/install)
- Git: [Installation Guide](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Installation

1. Clone the repository:

```bash
git clone https://github.com/hasan082/recipe-app.git
```

2. Change into the project directory:

```bash
cd recipe-app
```

3. Install the dependencies:

```bash
flutter pub get
```

4. Run the app:

```bash
flutter run
```
## Screenshot

<img src="https://gitlab.com/hasan082/live-test-9-ostad/-/raw/master/scree.png" width="280" />


## Resources

- Flutter Documentation: [https://flutter.dev/docs](https://flutter.dev/docs)
- Dart Documentation: [https://dart.dev/guides](https://dart.dev/guides)

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvement, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](https://opensource.org/license/mit/).
